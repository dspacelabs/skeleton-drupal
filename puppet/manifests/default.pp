####
#
# Puppet manifest for development in vagrant
#
$production_packages = [
    'git',
    'curl',
    'php-apc',
    'php5-curl',
    'php5-intl',
    'php5-sqlite',
    'php5-mysql',
    'php5-gd',
]
package { $production_packages:
    ensure  => present,
}
$development_packages = [
    'php5-xdebug',
    'vim',
]
package { $development_packages:
    ensure  => present,
}
#exec { "composer":
#    path      => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/opt/vagrant_ruby/bin",
#    creates   => "/usr/local/bin/composer.phar",
#    cwd       => "/usr/local/bin",
#    logoutput => true,
#    command   => "curl -sS https://getcomposer.org/installer | php",
#}
class { 'mysql::client': }
class { 'mysql::server': }
class { 'php': }
class { 'nginx': }
nginx::server { 'drupal':
    root     => '/var/www/drupal',
    template => 'nginx/drupal.erb',
}
