Skeleton Drupal Project
=======================

This project is for spinning up a new Drupal project as
quick as possible.

# Installation

    curl -sS https://getcomposer.org/installer | php
    php composer.phar --working-dir="puppet" install
    php composer.phar install
    vagrant up
    php bin/console database:create --vagrant

