<?php

namespace Dspacelabs\Component\DrupalSkeleton\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DatabaseCreateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('database:create')
            ->setDescription('Create database on server')
            ->setDefinition(
                array(
                    new InputOption('vagrant', null, InputOption::VALUE_NONE, 'Run on vagrant box'),
                    new InputOption('username', 'u', InputOption::VALUE_OPTIONAL, 'Mysql username', 'root'),
                    new InputOption('password', 'p', InputOption::VALUE_OPTIONAL, 'Mysql password', 'root'),
                    new InputArgument('database', InputArgument::OPTIONAL, 'Database name to be created', 'drupal'),
                )
            )
            ->setHelp(<<<DOC

Command that is used to create a database to be used
for wordpress

DOC
);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = sprintf(
            'mysqladmin -u%s -p%s create %s',
            $input->getOption('username'),
            $input->getOption('password'),
            $input->getArgument('database')
        );

        if ($input->hasOption('vagrant')) {
            $process = new Process(sprintf('vagrant ssh --command="%s"', $command));
        } else {
            $process = new Process($command);
        }

        $process->run(function ($type, $buffer) use($output) {
            $output->writeln($buffer);
        });
    }
}
